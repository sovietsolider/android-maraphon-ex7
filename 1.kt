import java.util.*

fun <firstColl, secondColl: MutableCollection<firstColl>> Collection<firstColl>.partitionTo(first: secondColl, second: secondColl, predicate: (firstColl) -> Boolean): Pair<secondColl, secondColl> {
    for (i in this)
    {
        if (predicate(i)) first.add(i)
        else second.add(i)
    }
    return Pair(first, second)
}


fun partitionWordsAndLines() {
    val (words, lines) = listOf("a", "a b", "c", "d e")
        .partitionTo(ArrayList(), ArrayList()) { s -> !s.contains(" ") }
    check(words == listOf("a", "c"))
    check(lines == listOf("a b", "d e"))
}

fun partitionLettersAndOtherSymbols() {
    val (letters, other) = setOf('a', '%', 'r', '}')
        .partitionTo(HashSet(), HashSet()) { c -> c in 'a'..'z' || c in 'A'..'Z' }
    check(letters == setOf('a', 'r'))
    check(other == setOf('%', '}'))
}
